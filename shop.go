package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"
)

type product struct {
	title string
	price float64
	unit  string
	ts    int64
}

type store struct {
	data map[int]product
}

var units = map[string]string{
	"шт": "штука",
	"уп": "упаковка",
	"гр": "грамм",
	"кг": "килограмм",
}

func (p product) Print() {
	fmt.Printf("Название: %s\n", p.title)
	fmt.Printf("Цена: %.2f/%s.\n", p.price, p.unit)
	fmt.Printf("Время добавления: %s\n",
		time.Unix(p.ts, 0).Format("2006-01-02 15:04:05"))
}

func (p product) Validate() error {
	if p.title == "" {
		return fmt.Errorf("Название продукта не может быть пустое")
	}
	if p.price <= 0 {
		return fmt.Errorf("Цена продукта должна быть больше 0")
	}
	if _, ok := units[p.unit]; !ok {
		return fmt.Errorf("Неизвестная еденица измерения")
	}
	return nil
}

func NewStore() store {
	store := store{}
	store.data = make(map[int]product)
	return store
}

func (db store) All() map[int]product {
	return db.data
}

func (db store) Print(data map[int]product) {
	if len(data) == 0 {
		fmt.Println("Продуктов не найдено...")
		return
	}

	type orderType struct {
		id    int
		title string
	}
	order := make([]orderType, 0, len(data))
	for id, p := range data {
		order = append(order, orderType{
			id:    id,
			title: p.title,
		})
	}
	sort.Slice(order, func(i, j int) bool {
		return order[i].title < order[j].title
	})

	fmt.Printf("%6s | %-36s | %3s | %8s | %s\n",
		"ID", "Название", "Ед.", "Цена (RUB)", "Дата изменения")
	for _, o := range order {
		p := data[o.id]
		fmt.Printf("%6d | %-36s | %-3s | %10.2f | %s\n",
			o.id, p.title, p.unit, p.price,
			time.Unix(p.ts, 0).Format("2006-01-02 15:04:05"))
	}
}

func (db store) Get(id int) (product, error) {
	if id <= 0 {
		return product{}, fmt.Errorf("Неверный ID продукта")
	}
	if _, ok := db.data[id]; !ok {
		return product{}, fmt.Errorf("Удаляемый продукт не найден")
	}
	return db.data[id], nil
}

func (db store) Search(s string) map[int]product {
	res := make(map[int]product)
	s = strings.ToLower(s)
	for id, p := range db.data {
		if strings.Contains(strings.ToLower(p.title), s) {
			res[id] = p
		}
	}
	return res
}

func (db *store) Add(p product) (id int, err error) {
	if err := p.Validate(); err != nil {
		return 0, err
	}
	id = len(db.data) + 1
	p.title = strings.Title(p.title)
	p.ts = time.Now().Unix()
	db.data[id] = p
	return id, nil
}

func (db *store) Del(id int) error {
	if id <= 0 {
		return fmt.Errorf("Неверный ID продукта")
	}
	if _, ok := db.data[id]; !ok {
		return fmt.Errorf("Удаляемый продукт не найден")
	}
	delete(db.data, id)
	return nil
}

func (db *store) Clear() {
	db.data = make(map[int]product)
}

func (db *store) Load(filename string) error {
	csvFile, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer csvFile.Close()
	csvLines, err := csv.NewReader(csvFile).ReadAll()
	if err != nil {
		return err
	}
	db.Clear()
	for _, line := range csvLines {
		if len(line) != 4 {
			continue
		}
		price, err := strconv.ParseFloat(line[1], 64)
		if err != nil {
			continue
		}
		ts, err := strconv.ParseInt(line[3], 10, 64)
		if err != nil {
			continue
		}
		p := product{
			title: line[0],
			price: price,
			unit:  line[2],
			ts:    ts,
		}
		if p.Validate() != nil {
			continue
		}
		db.data[len(db.data)+1] = p
	}
	return nil
}

func (db store) Save(filename string) error {
	csvFile, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer csvFile.Close()

	csvwriter := csv.NewWriter(csvFile)
	for _, p := range db.data {
		_ = csvwriter.Write([]string{
			p.title, fmt.Sprintf("%.2f", p.price),
			p.unit, fmt.Sprintf("%d", p.ts),
		})
	}
	csvwriter.Flush()
	return nil
}

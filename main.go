package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var products store

func help() {
	cmds := map[string]string{
		"показать":       "показать каталог продуктов",
		"показать {id}":  "информация о продукте с ID",
		"поиск {string}": "поиск в названии продукта",
		"добавить":       "добавить новый продукт",
		"удалить {id}":   "удалить продукт с ID",
		"загрузить":      "загрузить продукты из файла products.csv",
		"cохранить":      "сохранить продукты в файл products.csv",
		"очистить":       "очистить список продуктов",
		"помощь":         "вывод доступных комманд",
		"выход":          "завершение программы",
	}
	for k := range cmds {
		fmt.Printf("%16s - %s\n", k, cmds[k])
	}
}

func print(id string) {
	pid, err := strconv.Atoi(id)
	if err != nil {
		fmt.Println("[ОШИБКА] неверный ID продукта")
		return
	}
	p, err := products.Get(pid)
	if err != nil {
		fmt.Println("[ОШИБКА]", err)
	}
	p.Print()
}

func add() {
	fmt.Println("Добавление продукта")
	p := product{}

	fmt.Print("Название:\n> ")
	reader := bufio.NewReader(os.Stdin)

	input, _, _ := reader.ReadLine()
	p.title = string(input)

	fmt.Println("Выберите еденицу измерения:")
	for k := range units {
		fmt.Printf(" '%s' - %s\n", k, units[k])
	}
	fmt.Print("> ")
	fmt.Scanln(&p.unit)

	var (
		price string
		err   error
	)
	fmt.Print("Цена:\n> ")
	fmt.Scanln(&price)
	p.price, err = strconv.ParseFloat(price, 64)
	if err != nil {
		p.price = 0
	}

	id, err := products.Add(p)
	if err != nil {
		fmt.Println("[ОШИБКА]", err)
		return
	}
	fmt.Println("Продукт добавлен с ID", id)
}

func del(id string) {
	pid, err := strconv.Atoi(id)
	if err != nil {
		fmt.Println("[ОШИБКА] неверный ID продукта")
		return
	}
	err = products.Del(pid)
	if err != nil {
		fmt.Println("[ОШИБКА]", err)
		return
	}
	fmt.Printf("Продукт с ID %d удален\n", pid)
}

func main() {
	products = NewStore()
	fmt.Println("Каталог продуктов")

	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print(">> ")
		input, _, _ := reader.ReadLine()
		cmd := strings.Split(string(input), " ")
		if len(cmd) == 0 {
			continue
		}
		switch cmd[0] {
		case "добавить":
			add()
		case "поиск":
			q := strings.Join(cmd[1:], " ")
			d := products.Search(q)
			products.Print(d)
		case "показать":
			if len(cmd) == 2 {
				print(cmd[1])
			} else {
				d := products.All()
				products.Print(d)
			}
		case "удалить":
			if len(cmd) != 2 {
				fmt.Println("[ОШИБКА] не указан ID продукта")
				break
			}
			del(cmd[1])
		case "загрузить":
			err := products.Load("products.csv")
			if err != nil {
				fmt.Println("[ОШИБКА] загрузка продуктов", err)
				break
			}
			fmt.Println("Загружено успешно")
		case "сохранить":
			err := products.Save("products.csv")
			if err != nil {
				fmt.Println("[ОШИБКА] сохранение продуктов", err)
				break
			}
			fmt.Println("Сохранено успешно")
		case "помощь":
			help()
		case "выход":
			return
		default:
			fmt.Printf("Неподдерживаемая команда '%s'!\n", cmd[0])
			fmt.Println("Введите 'помощь' для вывода поддерживаемых команд")
		}
	}
}
